# == Schema Information
#
# Table name: orders
#
#  id               :integer          not null, primary key
#  status           :string
#  total_price      :decimal(6, 2)    default(0.0)
#  delivery_address :string
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  user_id          :integer
#
# Indexes
#
#  index_orders_on_user_id  (user_id)
#

require 'rails_helper'

RSpec.describe Order, type: :model do
  describe '#total_price' do
    it 'returns total price calculated from order items' do
      order = Order.new
      food = Food.new(name: 'curry', price: 10.00)
      order.order_items << OrderItem.new(order: order, quantity: 2, food: food)

      expect(order.total_price).to eq(20.00)
    end
  end
end
