require 'rails_helper'

RSpec.describe 'Payments callback API' do
  describe 'POST /payments' do
    it 'accepts payment notification from third party and creates an invoice' do
      user = User.create(email: 'john@example.com', password: '123456')
      food = Food.create(name: 'Fried Rice', price: 5.00)
      order = Order.create(delivery_address: 'Orchard Rd', order_items_attributes: [{quantity: 1, food: food}], user: user)
      words = "#{order.id}50"

      post '/payments', params: { order_id: order.id, amount: order.total_price, message: 'SUCCESS', channel: 'CREDIT_CARD', words: words }

      expect(json['status']).to eq('success')
      expect(json['data']['invoice']).to be_present
    end
  end
end
