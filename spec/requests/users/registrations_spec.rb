require 'rails_helper'

RSpec.describe 'User Registration API', type: :request do
  describe 'POST /users/register' do
    it 'creates user with valid params' do
      post '/users/register', params: { user: { name: 'Alixe', email: 'alixe@example.com', password: '123456' } }

      expect(json['status']).to eq('success')
      expect(json['data']['user']['auth_token']).to be_present
    end

    it 'returns failure without password' do
      post '/users/register', params: { user: { name: 'Alixe', email: 'alixe@example.com', password: '' } }

      expect(json['status']).to eq('fail')
      expect(json['data']).to eq({ 'password' => ["can't be blank"] })
    end
  end
end
