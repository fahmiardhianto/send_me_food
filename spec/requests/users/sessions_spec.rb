require 'rails_helper'

RSpec.describe 'User Session API', type: :request do
  describe 'POST /users/sign_in' do
    before do
      User.create(name: 'Bob', email: 'bob@example.com', password: 'secret')
    end

    it 'returns signed in user with auth_token' do
      post '/users/sign_in', params: { user: { email: 'bob@example.com', password: 'secret' } }

      expect(json['status']).to eq('success')
      expect(json['data']['user']['auth_token']).to be_present
    end

    it 'returns different token for every subsequent login' do
      post '/users/sign_in', params: { user: { email: 'bob@example.com', password: 'secret' } }

      first_token = json['data']['user']['auth_token']

      post '/users/sign_in', params: { user: { email: 'bob@example.com', password: 'secret' } }

      second_token = json['data']['user']['auth_token']

      expect(first_token).not_to eq(second_token)
    end

    it 'returns fail when password did not match' do
      post '/users/sign_in', params: { user: { email: 'bob@example.com', password: '123456' } }

      expect(json['status']).to eq('fail')
      expect(json['data']['password']).to eq("didn't match")
    end

    it 'returns fail when user not found' do
      post '/users/sign_in', params: { user: { email: 'alice@example.com', password: '123456' } }

      expect(json['status']).to eq('fail')
      expect(json['data']['user']).to eq('not found')
    end
  end
end
