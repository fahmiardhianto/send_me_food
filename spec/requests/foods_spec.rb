require 'rails_helper'

RSpec.describe 'Foods', type: :request do
  describe 'GET /foods' do
    it 'returns list of foods' do
      Food.create([
        {
          name: 'fried rice',
          price: 3.00
        },
        {
          name: 'chicken katsu',
          price: 2.50
        }
      ])

      get '/foods'

      expect(json['data']['foods'].count).to eq(2)
    end
  end

  describe 'POST /foods' do
    it 'creates food' do
      post '/foods', params: { food: { name: 'Katsu', price: 7.99 } }

      expect(json['status']).to eq('success')
      expect(json['data']['food']['name']).to eq('Katsu')
      expect(json['data']['food']['price'].to_f).to eq(7.99)
    end
  end
end
