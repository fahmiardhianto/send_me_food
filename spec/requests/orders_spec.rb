require 'rails_helper'

RSpec.describe 'Orders' do
  describe 'POST /orders' do
    it 'creates order' do
      food = Food.create(name: 'Curry', price: 2.00)
      user = User.create(name: 'Charlie', email: 'charlie@example.com', password: '123456')

      post '/orders', params: { order: { order_items_attributes: [{ food_id: food.id, quantity: 2 }], delivery_address: 'Orchard Rd' } }, headers: { 'Authorization' => user.auth_token}

      expect(json['status']).to eq('success')
      expect(json['data']['order']['total_price']).to eq(4.00.to_s)
      expect(json['data']['order']['user_id']).to eq(user.id)
    end

    it 'returns fail when numerical validation on quantity failed' do
      food = Food.create(name: 'Curry', price: 2.00)
      user = User.create(name: 'Charlie', email: 'charlie@example.com', password: '123456')

      post '/orders', params: { order: { order_items_attributes: [{ food_id: food.id }], delivery_address: 'Orchard Rd' } }, headers: { 'Authorization' => user.auth_token}

      expect(json['status']).to eq('fail')
      expect(json['data']['order_items.quantity']).to eq(['is not a number'])
    end

    it 'returns fail when quantity is 0' do
      food = Food.create(name: 'Curry', price: 2.00)
      user = User.create(name: 'Charlie', email: 'charlie@example.com', password: '123456')

      post '/orders', params: { order: { order_items_attributes: [{ food_id: food.id, quantity: 0 }], delivery_address: 'Orchard Rd' } }, headers: { 'Authorization' => user.auth_token}

      expect(json['status']).to eq('fail')
      expect(json['data']['order_items.quantity']).to eq(['must be greater than 0'])
    end
  end
end
