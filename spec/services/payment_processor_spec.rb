require 'rails_helper'

RSpec.describe 'Payment Processor' do
  context 'when validation fails' do
    it 'returns error when the order is not found' do
      payment_processor = PaymentProcessor.call(order_id: 99, amount: 10.00, message: 'SUCCESS', channel: 'CREDIT_CARD', words: "991000CREDIT_CARD")

      expect(payment_processor).to be_failure
      expect(payment_processor.errors).to eq({order: ["not found"]})
    end

    it 'returns error when the words is invalid' do
      user = User.create(email: 'john@example.com', password: '123456')
      food = Food.create(name: 'Fried Rice', price: 10.00)
      order = Order.create(delivery_address: 'Orchard Rd', order_items_attributes: [{quantity: 1, food: food}], user_id: user.id)
      payment_processor = PaymentProcessor.call(order_id: order.id, amount: order.total_price.to_s, message: 'SUCCESS', channel: 'CREDIT_CARD', words: "ABC123")

      expect(payment_processor).to be_failure
      expect(payment_processor.errors).to eq({words: ["is invalid"]})
    end
  end

  context 'when validation success' do
    before do
      user = User.create(email: 'john@example.com', password: '123456')
      food = Food.create(name: 'Fried Rice', price: 10.00)
      @order = Order.create(delivery_address: 'Orchard Rd', order_items_attributes: [{quantity: 1, food: food}], user_id: user.id)

      @payment_processor = PaymentProcessor.call(order_id: @order.id, amount: '10.00', message: 'SUCCESS', channel: 'CREDIT_CARD', words: "#{@order.id}100")
    end

    it 'returns an invoice when payment is valid' do
      expect(@payment_processor).to be_success
      expect(@payment_processor.result.class).to eq(Invoice)
      expect(@payment_processor.result.amount).to eq(10.0)
    end

    it 'update the order status to paid' do
      expect(@order.reload.status).to eq('paid')
    end
  end
end
