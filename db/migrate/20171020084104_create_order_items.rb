class CreateOrderItems < ActiveRecord::Migration[5.1]
  def change
    create_table :order_items do |t|
      t.references :order, foreign_key: true
      t.references :food, foreign_key: true
      t.string :status
      t.integer :quantity
      t.decimal :price, default: 0, precision: 6, scale: 2
      t.decimal :total_price, default: 0, precision: 6, scale: 2

      t.timestamps
    end
  end
end
