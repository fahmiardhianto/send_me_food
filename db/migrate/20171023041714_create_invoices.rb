class CreateInvoices < ActiveRecord::Migration[5.1]
  def change
    create_table :invoices do |t|
      t.decimal :amount, default: 0, precision: 6, scale: 2
      t.string :channel
      t.integer :order_id
      t.integer :user_id

      t.timestamps
    end

    add_index :invoices, :order_id
    add_index :invoices, :user_id
  end
end
