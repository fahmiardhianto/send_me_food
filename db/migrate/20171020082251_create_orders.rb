class CreateOrders < ActiveRecord::Migration[5.1]
  def change
    create_table :orders do |t|
      t.string :status
      t.decimal :total_price, default: 0, precision: 6, scale: 2
      t.string :delivery_address

      t.timestamps
    end
  end
end
