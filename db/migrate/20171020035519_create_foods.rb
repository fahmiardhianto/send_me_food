class CreateFoods < ActiveRecord::Migration[5.1]
  def change
    create_table :foods do |t|
      t.string :name
      t.decimal :price, default: 0, precision: 6, scale: 2

      t.timestamps
    end
  end
end
