foods = Food.create([
  {
    name: 'Fried Rice',
    price: 2.50
  },
  {
    name: 'Curry',
    price: 2.00
  },
  {
    name: 'Noodle',
    price: 3.00
  },
  {
    name: 'Fish and chips',
    price: 3.50
  },
  {
    name: 'Soup',
    price: 2.00
  }
])
