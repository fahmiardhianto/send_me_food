Rails.application.routes.draw do
  post '/users/register', to: 'users/registrations#create'
  post '/users/sign_in', to: 'users/sessions#create'

  get '/foods', to: 'foods#index'
  post '/foods', to: 'foods#create'

  get '/orders', to: 'orders#index'
  post '/orders', to: 'orders#create'
  get '/orders/:id', to: 'orders#show'

  post '/payments', to: 'payments#notify'
end
