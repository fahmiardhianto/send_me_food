# == Schema Information
#
# Table name: order_items
#
#  id          :integer          not null, primary key
#  order_id    :integer
#  food_id     :integer
#  status      :string
#  quantity    :integer
#  price       :decimal(6, 2)    default(0.0)
#  total_price :decimal(6, 2)    default(0.0)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_order_items_on_food_id   (food_id)
#  index_order_items_on_order_id  (order_id)
#
# Foreign Keys
#
#  fk_rails_...  (food_id => foods.id)
#  fk_rails_...  (order_id => orders.id)
#

class OrderItemSerializer < ActiveModel::Serializer
  attributes :quantity, :price, :food_id, :food_name, :total_price

  def food_name
    object.food.name
  end
end
