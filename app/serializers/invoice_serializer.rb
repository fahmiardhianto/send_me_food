class InvoiceSerializer < ActiveModel::Serializer
  attributes :id, :amount, :channel, :order_id
end
