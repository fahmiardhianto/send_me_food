# == Schema Information
#
# Table name: orders
#
#  id               :integer          not null, primary key
#  status           :string
#  total_price      :decimal(6, 2)    default(0.0)
#  delivery_address :string
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  user_id          :integer
#
# Indexes
#
#  index_orders_on_user_id  (user_id)
#

class OrderSerializer < ActiveModel::Serializer
  attributes :id, :delivery_address, :total_price, :status, :user_id, :order_items

  has_many :order_items

  def user_id
    object.user.id
  end
end
