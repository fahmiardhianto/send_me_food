class OrdersController < ApplicationController
  before_action :authenticate_request

  def index
    orders = @current_user.orders
    response = {}

    response = { status: 'success', data: { orders: orders.map { |order| OrderSerializer.new(order) } } }

    render json: response
  end

  def show
    order = @current_user.orders.find_by(id: params[:id])
    response = {}

    if order.present?
      response = { status: 'success', data: { order: OrderSerializer.new(order) } }
    else
      response = { status: 'fail', data: 'Order associated with the user not found' }
    end
    render json: response
  end

  def create
    order = Order.new(order_params)
    order.user = @current_user
    response = {}

    if order.save
      response = { status: 'success', data: { order: OrderSerializer.new(order) } }
    else
      response = { status: 'fail', data: order.errors }
    end
    render json: response
  end

  private

  def order_params
    params.require(:order).permit(:delivery_address, order_items_attributes: [:quantity, :food_id])
  end
end
