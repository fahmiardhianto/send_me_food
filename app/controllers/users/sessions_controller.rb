class Users::SessionsController < ApplicationController
  def create
    user = User.find_by(email: params[:user][:email])
    response = {}

    if user.nil?
      response = { status: 'fail', data: { user: 'not found' } }
    elsif user.try(:authenticate, params[:user][:password])
      user.regenerate_auth_token
      response = { status: 'success', data: { user: UserSerializer.new(user) } }
    else
      response = { status: 'fail', data: { password: "didn't match" } }
    end

    render json: response
  end
end
