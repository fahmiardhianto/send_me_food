class Users::RegistrationsController < ApplicationController
  def create
    user = User.new(user_params)
    response = {}

    if user.save
      response = { status: 'success', data: { user: UserSerializer.new(user) } }
    else
      response = { status: 'fail', data: user.errors.messages }
    end
    render json: response
  end

  private

  def user_params
    params.require(:user).permit(:name, :email, :password)
  end
end
