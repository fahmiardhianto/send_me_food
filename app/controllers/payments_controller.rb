class PaymentsController < ApplicationController
  def notify
    payment_processor = PaymentProcessor.call(
      order_id: params[:order_id],
      amount: params[:amount],
      message: params[:message],
      channel: params[:channel],
      words: params[:words]
    )
    response = {}

    if payment_processor.success?
      response = { status: 'success', data: { invoice: InvoiceSerializer.new(payment_processor.result) } }
    else
      response = { status: 'fail', data: payment_processor.errors }
    end
    render json: response
  end
end
