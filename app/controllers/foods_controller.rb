class FoodsController < ApplicationController
  def index
    foods = Food.all

    render json: { status: 'success', data: { foods: foods.map { |f| FoodSerializer.new(f) } } }
  end

  def create
    food = Food.new(food_params)
    response = {}

    if food.save
      response = { status: 'success', data: { food: FoodSerializer.new(food) }}
    else
      response = { status: 'fail', data: food.errors }
    end
    render json: response
  end

  private

  def food_params
    params.require(:food).permit(:name, :price)
  end
end
