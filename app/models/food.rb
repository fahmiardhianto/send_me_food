# == Schema Information
#
# Table name: foods
#
#  id         :integer          not null, primary key
#  name       :string
#  price      :decimal(6, 2)    default(0.0)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Food < ApplicationRecord
end
