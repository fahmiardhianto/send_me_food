# == Schema Information
#
# Table name: invoices
#
#  id         :integer          not null, primary key
#  amount     :decimal(6, 2)    default(0.0)
#  channel    :string
#  order_id   :integer
#  user_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_invoices_on_order_id  (order_id)
#  index_invoices_on_user_id   (user_id)
#

class Invoice < ApplicationRecord
  belongs_to :user
  belongs_to :order
end
