# == Schema Information
#
# Table name: orders
#
#  id               :integer          not null, primary key
#  status           :string
#  total_price      :decimal(6, 2)    default(0.0)
#  delivery_address :string
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  user_id          :integer
#
# Indexes
#
#  index_orders_on_user_id  (user_id)
#

class Order < ApplicationRecord
  include AASM

  aasm column: 'status' do
    state :placed, initial: true
    state :paid
    state :delivered

    event :pay do
      transitions from: :placed, to: :paid
    end

    event :deliver do
      transitions from: :paid, to: :delivered
    end
  end

  has_many :order_items
  accepts_nested_attributes_for :order_items
  belongs_to :user

  validates :order_items, presence: true

  def total_price
    order_items.reduce(0) { |total, i| i.total_price }
  end
end
