class PaymentProcessor
  prepend SimpleCommand

  def initialize(order_id:, amount:, message:, channel:, words:)
    @order   = Order.find_by(id: order_id)
    @amount  = amount
    @message = message
    @channel = channel
    @words   = words
  end

  def call
    if @order.nil?
      errors.add(:order, 'not found')
      return
    end

    if words_invalid?
      errors.add(:words, 'is invalid')
      return
    end

    if @message.eql?('SUCCESS')
      @order.pay!
      invoice = Invoice.create(amount: @order.total_price, channel: @channel, order: @order, user: @order.user)
      return invoice
    end

    nil
  end

  def words_invalid?
    !@words.eql?(words_from_order)
  end

  def words_from_order
    "#{@order.id}#{@order.total_price.to_s.gsub('.','')}"
  end
end
