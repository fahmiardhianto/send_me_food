class AuthorizeApiRequest
  def initialize(headers = {})
    @headers = headers
  end

  def call
    user
  end

  private

  def user
    @user ||= User.find_by(auth_token: http_auth_header)
  end

  def http_auth_header
    return @headers['Authorization'].split(' ').last if @headers['Authorization'].present?
  end
end
