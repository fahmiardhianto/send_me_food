# README

* Ruby version

   `2.3.1`

* Configuration
  
   Rename `config/example.application.yml` into `config/application.yml`  
   
   Update database url in `config/application.yml`

* Database creation
  
   Run `rails db:create` and `rails db:migrate` to create development database.  
   
* Database initialization

   To populate the database with seed data, run `rails db:seed`.   
   
* How to run the test suite

   Run `RAILS_ENV=test rails db:create` and `RAILS_ENV=test rails db:migrate` to create test database.  
   
   Run the test suite using `bundle exec rspec` command.

* Start development server

   Run `rails server` from terminal on project directory.
